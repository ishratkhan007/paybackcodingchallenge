# Coding challenge by Payback
This project perform image search using Pixbay API

This is a sample app to demonstrate a clean architecture for building apps on android.You need to apply for api from https://pixabay.com/api/docs/#api_search_images to run this project.

User can perform search on pixbay api, in result user will get thumb , tags, author details, returned data will be stored on Room database.

# Project Architecture
* Kotlin is the main programming language in the project.
* This project is based on MVVM + data binding + Clean architecture.
* Koin is used to inject dependencies.
* Persistence is provided by ROOM
* Android ViewModel library is used for implementing MVVM architecture.
* DataBinding is used to propagate changes in the UI


## Getting Started

Clone the project and open it in android studio. Add the api key in the project's `local.properties` file


```
api.key="<YOUR API KEY>"
```

## Built With
* [Koin](https://insert-koin.io/) - Dependency Injection Management
* [Retrofit](https://square.github.io/retrofit/) - Networking client for network calls
* [Room](https://developer.android.com/topic/libraries/architecture/room) - Database used for persistence
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Helper framework to implement MVVM architecture
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Lifecycle aware observable framework