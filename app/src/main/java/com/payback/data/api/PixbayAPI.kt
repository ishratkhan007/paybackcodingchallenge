package com.payback.data.api

import com.payback.data.vo.ApiResponseVo
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * REST API access point
 */
interface PixbayAPI {

    @GET("https://pixabay.com/api/")
    fun searchImageAsync(@QueryMap queryMap: Map<String, String>): Deferred<ApiResponseVo>
}