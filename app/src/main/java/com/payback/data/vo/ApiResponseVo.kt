package com.payback.data.vo

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Fts4
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class ApiResponseVo(
    @SerializedName("hits")
    val searchDataVo: List<SearchDataVo>,
    @SerializedName("total")
    val total: Int,
    @SerializedName("totalHits")
    val totalHits: Int
)

@Fts4
@Entity(tableName = "PIXBAY_IMAGES_TABLE")
@Parcelize
data class SearchDataVo(
    @PrimaryKey
    @ColumnInfo(name = "rowid")
    val rowId: Int,
    @SerializedName("comments")
    val comments: Int? = null,
    @SerializedName("downloads")
    val downloads: Int? = null,
    @SerializedName("favorites")
    val favorites: Int? = null,
    @SerializedName("fullHDURL")
    val fullHDURL: String? = null,
    @SerializedName("id")
    val id: Int,
    @SerializedName("imageHeight")
    val imageHeight: Int? = null,
    @SerializedName("imageSize")
    val imageSize: Int? = null,
    @SerializedName("imageURL")
    val imageURL: String? = null,
    @SerializedName("imageWidth")
    val imageWidth: Int? = null,
    @SerializedName("largeImageURL")
    val largeImageURL: String? = null,
    @SerializedName("likes")
    val likes: Int? = null,
    @SerializedName("pageURL")
    val pageURL: String? = null,
    @SerializedName("previewHeight")
    val previewHeight: Int? = null,
    @SerializedName("previewURL")
    val previewURL: String? = null,
    @SerializedName("previewWidth")
    val previewWidth: Int? = null,
    @SerializedName("tags")
    val tags: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("user")
    val user: String? = null,
    @SerializedName("user_id")
    val userId: Int? = null,
    @SerializedName("userImageURL")
    val userImageURL: String? = null,
    @SerializedName("views")
    val views: Int? = null,
    @SerializedName("webformatHeight")
    val webformatHeight: Int? = null,
    @SerializedName("webformatURL")
    val webformatURL: String? = null,
    @SerializedName("webformatWidth")
    val webformatWidth: Int? = null
) : Parcelable