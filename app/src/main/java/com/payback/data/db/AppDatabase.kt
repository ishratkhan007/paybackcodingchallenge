package com.payback.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.payback.data.db.dao.PixbayImageDao
import com.payback.data.vo.SearchDataVo

@Database(entities = [SearchDataVo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun imageTableDao(): PixbayImageDao
}