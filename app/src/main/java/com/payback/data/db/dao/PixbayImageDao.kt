package com.payback.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.payback.data.vo.SearchDataVo

@Dao
interface PixbayImageDao {

    @Query("SELECT *,rowid FROM PIXBAY_IMAGES_TABLE WHERE tags MATCH :query LIMIT 20 OFFSET :offset")
    fun searchImage(query: String, offset: Int): List<SearchDataVo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(searchDataVo: List<SearchDataVo>)

}