package com.payback.data.repo

import com.payback.data.vo.ApiResponseVo

class DataRepo(private val networkRepo: NetworkRepo, private val localRepo: LocalRepo) {

    suspend fun searchImage(query: String, page: Int): ApiResponseVo {
        val cacheData = localRepo.searchImage(query, page)
        return if (cacheData.isEmpty()) {
            val apiResponseVo = networkRepo.searchImage(query, page)
            localRepo.insertAll(apiResponseVo.searchDataVo)
            apiResponseVo
        } else {
            ApiResponseVo(cacheData, -1, -1)
        }
    }
}