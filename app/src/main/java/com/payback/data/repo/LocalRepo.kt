package com.payback.data.repo

import com.payback.data.db.dao.PixbayImageDao
import com.payback.data.vo.SearchDataVo
import com.payback.utils.PAGE_SIZE

class LocalRepo(private val imageTableDao: PixbayImageDao) {

    fun searchImage(query: String, page: Int): List<SearchDataVo> {
        val offset = (page - 1) * PAGE_SIZE

        return if (query.length > 1)
            imageTableDao.searchImage(
                query.replaceRange(
                    query.length - 1, query.length, "*"
                )
                , offset
            )
        else imageTableDao.searchImage(query, offset)
    }

    fun insertAll(searchDataVo: List<SearchDataVo>) = imageTableDao.insertAll(searchDataVo)
}