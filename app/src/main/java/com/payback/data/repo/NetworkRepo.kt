package com.payback.data.repo

import com.payback.BuildConfig
import com.payback.data.api.PixbayAPI
import com.payback.data.vo.ApiResponseVo
import com.payback.utils.PAGE_SIZE


class NetworkRepo(private val api: PixbayAPI) {

    suspend fun searchImage(query: String, page: Int): ApiResponseVo {
        return api.searchImageAsync(
            mapOf(
                Pair("key", BuildConfig.API_KEY),
                Pair("q", query),
                Pair("image_type", "photo"),
                Pair("page", page.toString()),
                Pair("per_page", PAGE_SIZE.toString())
            )
        ).await()
    }
}