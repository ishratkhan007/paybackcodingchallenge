package com.payback.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.payback.data.api.PixbayAPI
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *
 * Api documentation
 * https://pixabay.com/api/docs/#api_search_images
 *
 * */

private const val DEFAULT_READ_TIME_OUT = 120
private const val DEFAULT_CONNECT_TIME_OUT = 120
private const val BASE_URL = "https://pixabay.com/api/"

val networkModule = module {

    single { createOkHttpClient() }
    single { createWebService(get()) }
}

fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .readTimeout(DEFAULT_READ_TIME_OUT.toLong(), TimeUnit.SECONDS)
        .connectTimeout(DEFAULT_CONNECT_TIME_OUT.toLong(), TimeUnit.SECONDS).build()
}

fun createWebService(okHttpClient: OkHttpClient): PixbayAPI {
    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    return retrofit.create(PixbayAPI::class.java)
}