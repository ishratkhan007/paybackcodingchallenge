package com.payback.di

import androidx.room.Room
import com.payback.data.db.AppDatabase
import com.payback.data.repo.DataRepo
import com.payback.data.repo.LocalRepo
import com.payback.data.repo.NetworkRepo
import com.payback.ui.search.ImageSearchDataSourceFactory
import com.payback.ui.search.SearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val koinModules = module {

    single { NetworkRepo(get()) }
    single { DataRepo(get(), get()) }
    single { ImageSearchDataSourceFactory(get()) }
    single { LocalRepo(get<AppDatabase>().imageTableDao()) }

    viewModel { SearchViewModel(get()) }
}

val roomModule = module {
    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "pixbay_database")
            .build()
    }
}
