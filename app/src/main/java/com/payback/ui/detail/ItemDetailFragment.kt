package com.payback.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.payback.databinding.FragmentItemDetailBinding
import com.payback.utils.ARG_KEY_SEARCH_VO

class ItemDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val itemDetailBinding: FragmentItemDetailBinding =
            FragmentItemDetailBinding.inflate(inflater, container, false)
        itemDetailBinding.lifecycleOwner = viewLifecycleOwner
        itemDetailBinding.searchedItem = arguments?.getParcelable(ARG_KEY_SEARCH_VO)

        return itemDetailBinding.root
    }

}