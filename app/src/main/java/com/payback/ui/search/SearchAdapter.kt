package com.payback.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.payback.data.vo.SearchDataVo
import com.payback.databinding.ItemSearchBinding
import com.payback.utils.OnItemClickEvent


class SearchAdapter(private val onSearchItemClick: OnItemClickEvent) :
    PagedListAdapter<SearchDataVo, SearchViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ItemSearchBinding =
            ItemSearchBinding.inflate(layoutInflater, parent, false)
        return SearchViewHolder(itemBinding, onSearchItemClick)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it) }
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<SearchDataVo>() {
            override fun areItemsTheSame(oldItem: SearchDataVo, newItem: SearchDataVo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: SearchDataVo, newItem: SearchDataVo): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}