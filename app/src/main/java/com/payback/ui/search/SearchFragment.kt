package com.payback.ui.search

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.payback.R
import com.payback.databinding.FragmentSearchImageBinding
import com.payback.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

const val REQ_CODE_SPEECH_INPUT = 90

class SearchFragment : Fragment() {

    private val searchViewModel: SearchViewModel by viewModel()
    private var adapter: SearchAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        adapter = SearchAdapter(onSearchItemClick())

        val fragmentSearchImageBinding =
            FragmentSearchImageBinding.inflate(inflater, container, false)

        fragmentSearchImageBinding.searchAdapter = adapter
        fragmentSearchImageBinding.lifecycleOwner = viewLifecycleOwner
        fragmentSearchImageBinding.searchViewModel = searchViewModel
        fragmentSearchImageBinding.searchFragment = this
        fragmentSearchImageBinding.decoration1stItem = DecoratorFirstLastItem(
            spanCount = if (context?.resources?.getBoolean(R.bool.isLandscape) == true) 4 else 2,
            firstTopPadding = 80.dpToPx(),
            lastBottom = 20.dpToPx()
        )

        searchViewModel.pagedList.observe(viewLifecycleOwner, Observer {
            adapter?.submitList(it)
        })

        return fragmentSearchImageBinding.root
    }

    private fun onSearchItemClick(): OnItemClickEvent = { view, item ->
        MaterialAlertDialogBuilder(context)
            .setTitle(getString(R.string.more_detail))
            .setMessage(getString(R.string.ask_about_more_detail))
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                findNavController().navigate(
                    R.id.action_navigation_home_to_itemDetailFragment,
                    bundleOf(ARG_KEY_SEARCH_VO to item)
                )
            }
            .setNegativeButton(getString(R.string.no)) { _, _ ->

            }.show()
    }

    fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.app_name))
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            getString(R.string.speech_not_supported).toToast(activity)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)[0]
                    searchViewModel.ediTextQuery.set(result)
                }
            }
        }
    }

}