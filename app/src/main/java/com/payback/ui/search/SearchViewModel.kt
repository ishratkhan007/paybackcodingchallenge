package com.payback.ui.search

import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.payback.BaseViewModel
import com.payback.UiState
import com.payback.data.vo.SearchDataVo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchViewModel(private val dataSourceFactory: ImageSearchDataSourceFactory) :
    BaseViewModel() {

    var pagedList: LiveData<PagedList<SearchDataVo>> = LivePagedListBuilder(
        dataSourceFactory, PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .setPrefetchDistance(5)
            .setInitialLoadSizeHint(20)
            .build()
    )
        .build()

    val ediTextQuery = ObservableField<String>()

    val searchClearButtonVisibility = ObservableField<Int>()
    val micButtonVisibility = ObservableField<Int>()

    init {
        searchClearButtonVisibility.set(View.GONE)
        micButtonVisibility.set(View.VISIBLE)

        ediTextQuery.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (sender == ediTextQuery)
                    ediTextQuery.get()?.let {
                        search(it)
                        searchClearButtonVisibility.set(if (it.isNotEmpty()) View.VISIBLE else View.GONE)
                        micButtonVisibility.set(if (it.isNotEmpty()) View.GONE else View.VISIBLE)
                    }
            }
        })
    }

    val uiStateLiveData: LiveData<UiState> = Transformations.switchMap(
        dataSourceFactory.mutableLiveData,
        ImageSearchDataSource::uiStateLiveData
    )

    fun clearSearch() {
        ediTextQuery.set("")
    }

    fun onTryAgainClicked() {
        ediTextQuery.get()?.let { search(it) } ?: run { search("") }
    }

    private val searchMsThreshold = 600L
    private var searchJob: Job? = null
    private fun search(query: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch(Dispatchers.IO) {
            delay(searchMsThreshold)
            dataSourceFactory.searchQueryUpdated(query)
        }
    }

    override fun onCleared() {
        super.onCleared()
        dataSourceFactory.clearCoroutine()
    }
}