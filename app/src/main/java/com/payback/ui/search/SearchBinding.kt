package com.payback.ui.search

import android.content.res.ColorStateList
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.payback.R


object SearchBinding {

    @JvmStatic
    @BindingAdapter("adapter")
    fun setAdapter(recyclerView: RecyclerView, searchAdapter: SearchAdapter) {
        recyclerView.layoutManager = StaggeredGridLayoutManager(
            if (recyclerView.context.resources.getBoolean(
                    R.bool.isLandscape
                )
            ) 4 else 2, GridLayoutManager.VERTICAL
        )
        recyclerView.adapter = searchAdapter
    }

    @JvmStatic
    @BindingAdapter(value = ["dimensionRatioW","dimensionRatioH"])
    fun dimensionRatio(view: ImageView, dimensionRatioW: Int, dimensionRatioH: Int) {
        val previewRatio =
            String.format("%d:%d", dimensionRatioW, dimensionRatioH)
        val set = ConstraintSet()
        val cLayout = view.parent as ConstraintLayout
        set.clone(cLayout)
        set.setDimensionRatio(view.id, previewRatio)
        set.applyTo(cLayout)
    }

    @JvmStatic
    @BindingAdapter("thumbUrl")
    fun loadImage(view: ImageView, imageUrl: String?) {
        Glide.with(view.context)
            .load(imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(view)
    }

    @JvmStatic
    @BindingAdapter("thumbRoundedUrl")
    fun loadRoundedImage(view: ImageView, imageUrl: String?) {
        Glide.with(view.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(view)
    }

    @JvmStatic
    @BindingAdapter("itemDecoration")
    fun setItemDecoration(view: RecyclerView, decor: ItemDecoration?) {
        if (decor != null) {
            view.addItemDecoration(decor)
        }
    }

    @JvmStatic
    @BindingAdapter("addTags")
    fun addTags(chipGroup: ChipGroup, tags: String) {
        if (chipGroup.isEmpty()) {
            tags.split(",").forEach {
                chipGroup.addView(Chip(chipGroup.context).apply {
                    text = it
                    chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            chipGroup.context,
                            R.color.offWhite
                        )
                    )
                })
            }
        }
    }
}