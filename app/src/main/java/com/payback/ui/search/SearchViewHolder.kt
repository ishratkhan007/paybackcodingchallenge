package com.payback.ui.search

import androidx.recyclerview.widget.RecyclerView
import com.payback.data.vo.SearchDataVo
import com.payback.databinding.ItemSearchBinding
import com.payback.utils.OnItemClickEvent

class SearchViewHolder(
    private val searchBinding: ItemSearchBinding,
    private val onSearchItemClick: OnItemClickEvent
) :
    RecyclerView.ViewHolder(searchBinding.root) {

    fun bindTo(searchDataVo: SearchDataVo) {
        searchBinding.searchedItem = searchDataVo
        searchBinding.executePendingBindings()
        searchBinding.root.setOnClickListener {
            onSearchItemClick.invoke(searchBinding.root, searchDataVo)
        }
    }
}