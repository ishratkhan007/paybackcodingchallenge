package com.payback.ui.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.payback.UiState
import com.payback.data.repo.DataRepo
import com.payback.data.vo.SearchDataVo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.net.ConnectException
import java.net.UnknownHostException

const val DEFAULT_QUERY = "fruits"

class ImageSearchDataSource(
    private val dataRepo: DataRepo,
    private var query: String
) :
    PageKeyedDataSource<Int, SearchDataVo>() {

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.IO + job)
    val uiStateLiveData = MutableLiveData<UiState>()

    fun clearCoroutine() {
        job.cancel()
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, SearchDataVo>
    ) {
        coroutineScope.launch {
            try {
                uiStateLiveData.postValue(UiState.ShowLoading)
                val apiResponse = dataRepo.searchImage(query, 1)
                uiStateLiveData.postValue(UiState.HideLoading)
                callback.onResult(apiResponse.searchDataVo, null, 2)

                if (apiResponse.searchDataVo.isEmpty()) {
                    uiStateLiveData.postValue(
                        UiState.ErrorState(
                            UiState.ErrorCode.NO_RESULT
                        )
                    )
                }

            } catch (exception: Exception) {
                Log.d("ImageSearchDataSource", exception.toString())
                handleException(exception)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, SearchDataVo>) {
        coroutineScope.launch {
            try {
                uiStateLiveData.postValue(UiState.ShowLoading)
                val response = dataRepo.searchImage(query, params.key)
                val nextKey = params.key + 1
                callback.onResult(response.searchDataVo, nextKey)
                uiStateLiveData.postValue(UiState.HideLoading)
            } catch (exception: Exception) {
//                handleException(exception)
            }
        }
    }

    private fun handleException(exception: Exception) {
        uiStateLiveData.postValue(
            when (exception) {
                is UnknownHostException,
                is ConnectException -> {
                    UiState.ErrorState(
                        UiState.ErrorCode.NO_INTERNET
                    )
                }
                else -> {
                    UiState.ErrorState(UiState.ErrorCode.TRY_AGAIN)
                }
            }
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, SearchDataVo>) {
    }
}


class ImageSearchDataSourceFactory(private val dataRepo: DataRepo) :
    DataSource.Factory<Int, SearchDataVo>() {

    val mutableLiveData: MutableLiveData<ImageSearchDataSource> = MutableLiveData()
    private var searchDataSource: ImageSearchDataSource? = null
    private var query = DEFAULT_QUERY

    override fun create(): DataSource<Int, SearchDataVo> {
        searchDataSource = ImageSearchDataSource(dataRepo, query)
        mutableLiveData.postValue(searchDataSource)
        return searchDataSource as ImageSearchDataSource
    }

    fun searchQueryUpdated(query: String) {
        this.query = if (query.isEmpty()) DEFAULT_QUERY else query
        searchDataSource?.invalidate()
    }

    fun clearCoroutine() = searchDataSource?.clearCoroutine()

}