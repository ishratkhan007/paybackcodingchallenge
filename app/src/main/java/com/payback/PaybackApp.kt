package com.payback

import android.app.Application
import com.payback.di.koinModules
import com.payback.di.networkModule
import com.payback.di.roomModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PaybackApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PaybackApp)
            modules(networkModule + koinModules + roomModule)
        }
    }
}