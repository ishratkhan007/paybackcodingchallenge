package com.payback

import androidx.lifecycle.ViewModel

sealed class UiState {
    object ShowLoading : UiState()
    object HideLoading : UiState()
    data class ErrorState(var error: ErrorCode) : UiState()

    enum class ErrorCode {
        NO_INTERNET,
        NO_RESULT,
        TRY_AGAIN
    }
}

open class BaseViewModel : ViewModel()