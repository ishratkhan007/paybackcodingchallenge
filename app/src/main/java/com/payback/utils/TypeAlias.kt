package com.payback.utils

import android.view.View
import com.payback.data.vo.SearchDataVo

internal typealias OnItemClickEvent = (view: View, item: SearchDataVo) -> Unit