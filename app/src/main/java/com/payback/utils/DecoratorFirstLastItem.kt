package com.payback.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

class DecoratorFirstLastItem(private val spanCount:Int=2, private val firstTopPadding: Int = 0, private val lastBottom: Int = 0) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        val isLast = position == state.itemCount - 1
        val isStaggered = parent.layoutManager is StaggeredGridLayoutManager

        if (isLast) {
            outRect.bottom = lastBottom
            outRect.top = 0 //don't forget about recycling...
        }

        if (position == 0 || (isStaggered && position < spanCount)) {
            outRect.top = firstTopPadding
        }
    }
}