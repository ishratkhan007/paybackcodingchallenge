package com.payback

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton

@BindingAdapter("uiLoadingState")
fun setUiStateForLoading(progressView: ProgressBar, uiState: UiState?) {
    progressView.visibility = when (uiState) {
        UiState.ShowLoading -> View.VISIBLE
        else -> View.GONE
    }
}

@BindingAdapter("uiMessageState")
fun setUiMessageState(textView: TextView, uiState: UiState?) {
    when (uiState) {
        is UiState.ErrorState -> {
            textView.visibility = View.VISIBLE
            when (uiState.error) {
                UiState.ErrorCode.NO_INTERNET -> {
                    textView.text = textView.context.getString(R.string.no_internet)
                }
                UiState.ErrorCode.NO_RESULT -> {
                    textView.text = textView.context.getString(R.string.no_response)
                }
            }
        }
        else ->
            textView.visibility = View.GONE
    }
}

@BindingAdapter("uiTryAgainButton")
fun setUiStatForTryAgainButton(button: MaterialButton, uiState: UiState?) {
    button.visibility = when (uiState) {
        is UiState.ErrorState -> {
            when (uiState.error) {
                UiState.ErrorCode.NO_INTERNET -> {
                    View.VISIBLE
                }
                UiState.ErrorCode.NO_RESULT -> {
                    View.GONE
                }
                UiState.ErrorCode.TRY_AGAIN -> {
                    View.VISIBLE
                }
            }
        }
        else ->
            View.GONE
    }
}
